require('dotenv').config();

const ApolloClientConfigDevelopment = {
  httpEndpoint: process.env.APP_GRAPHQL_ENDPOINT,
  httpLinkOptions: {
    headers: {
      [process.env.AUTH_SERVER_ADMIN_HEADER]: process.env.HASURA_GRAPHQL_ADMIN_SECRET,
    },
  },
};

const isProd = process.env.NODE_ENV === 'production';

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'hasura-as-baas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ['@nuxtjs/dotenv', '@nuxtjs/apollo'],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },

  // @nuxtjs/apollo configuration (https://github.com/nuxt-community/apollo-module)
  apollo: {
    clientConfigs: {
      default: isProd
        ? '~/graphql/apollo-client-config.js'
        : { ...ApolloClientConfigDevelopment },
    },
  },
}
