import gql from 'graphql-tag';

export const GetUser = gql`
  query GetUser($email: String!) {
    users(where: { email: { _eq: $email } }) {
      email
      name
      role
    }
  }
`;
