import gql from 'graphql-tag';

export const GetAllLists = gql`
  query GetAllLists {
    lists {
      name
      description
      products {
        product {
          name
          description
        }
      }
    }
  }
`;
