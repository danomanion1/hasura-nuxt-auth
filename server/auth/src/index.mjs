import dotenv from 'dotenv';
import fastify from 'fastify';
import pg from 'pg';

// Do not look for .env file in prod
if (process.env.NODE_ENV !== 'production') {
  const dotenvConf = Object.assign(
    {},
    process.env.ENV_FILE ? { path: process.env.ENV_FILE } : {},
  );
  dotenv.config(dotenvConf);
}

const { Pool } = pg;
// Assign envvars to constants
const {
  AUTH_SERVER_HOST: envHost,
  AUTH_SERVER_PORT: envPort,
  AUTH_SERVER_HEADER_KEY: headerKey,
  HASURA_GRAPHQL_DATABASE_URL: connectionString,
} = process.env;

const Roles = {
  REGISTERED: 'registered',
  ANONYMOUS: 'anonymous',
};
const defaultConf = {
  host: 'localhost',
  port: '3030',
};
const Strings = {
  SHUTDOWN_MESSAGE: 'Shutting down server!',
};
const Errors = {
  NO_DATA: 'No token was provided or payload was in the wrong format.',
  DEFAULT_ERROR: 'An error happened',
};
const { host, port } = Object.assign(
  {},
  defaultConf,
  // process.env,
  envHost ? { host: envHost } : {}, // Map from process env constants
  envPort ? { port: envPort } : {}, // Map from process env constants
);

// Read-only SQL
const getRoleQuery = ({ email }) =>
  `SELECT id, role FROM users WHERE email='${email}'`

class App {
  constructor({ host, port }) {
    const server = fastify({ logger: true });
    const pool = new Pool({ connectionString });

    this.server = server;
    this.host = host;
    this.port = port;
    this.pool = pool;

    this.init();
  }

  init() {
    const { pool, server } = this;

    // First and only route
    server.post('/', async (req, reply) => {
      const { headers } = req.body;

      if (!headers) {
        return reply.code(400).send(new Error(Errors.NO_DATA));
      }

      if (!headers[headerKey]) {
        reply.code(200).send({ 'X-Hasura-Role': Roles.ANONYMOUS });
      }

      try {
        const { rows } = await pool.query(getRoleQuery({ email: headers[headerKey] }));
        const user = rows.length > 0 ? rows[0]:  { role: Roles.ANONYMOUS };

        reply.code(200).send({
          'X-Hasura-Role': user.role,
          'X-Hasura-User-Id': user.id || ''
        });
      } catch (error) {
        return reply.code(400).send(error);
      }
    });
  }

  async startServer() {
    const { port, server, host } = this;
    try {
      await server.listen(port, host);
    } catch (error) {
      server.log.error(Errors.DEFAULT_ERROR, error);
      process.exit(1);
    }
  }

  // Exit gracefully, useful to reduce docker-compose shutdown time
  exit() {
    const { server } = this;
    server.close().then(
      () => {
        server.log.info(Strings.SHUTDOWN_MESSAGE);
      },
      (error) => {
        server.log.error(Errors.DEFAULT_ERROR, error);
      }
    )
  }
}

const app = new App({ host, port })

app.startServer()

process.on('SIGTERM', () => {
  app.exit()
})
